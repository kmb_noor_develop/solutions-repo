import java.util.*;


public class Main{

	private LinkedList<Integer> adj[];
	private int vCount;
	
	public Main(int v){
		vCount = v;
		adj = new LinkedList[v];
		for(int j=0; j<v; j++){
			adj[j] = new LinkedList<Integer>();
		}
	}
	
	public void addEdge(int v, int e){
		adj[v].add(e);
	}
	
	public void BFS(Integer s){	
		boolean visited[] = new boolean[vCount];
		LinkedList<Integer> queue = new LinkedList<>();	
		
		visited[s] = true;
		queue.add(s);
		
		while(queue.size() != 0){
			s = queue.poll();
			System.out.print(s+" ");
            
			Iterator<Integer> i = adj[s].listIterator();
			
			while(i.hasNext()){
				int n = i.next();
				if(!visited[n]){
					visited[n] = true;
					queue.add(n);
				}
			}			
		}				
	}
	
	
	public static void main(String[] args){
		Main g = new Main(4);
		g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);
 
        System.out.println("Following is Breadth First Traversal "+
                           "(starting from vertex 2)");
 
        g.BFS(1);
	}
}