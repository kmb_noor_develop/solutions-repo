import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Main {

    // Complete the staircase function below.
    static void staircase(int n) {
        for(int i=n; i>0; i--){
            String spaceStr = "";
            String hashes = "";
            
            for(int j=0; j<i-1; j++){
                spaceStr = spaceStr+" ";
                hashes = hashes+"#";
            }
            
            String out = (spaceStr+hashes).substring(0,n);
            
            System.out.println(out);
        }
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n =6;

        staircase(n);

        scanner.close();
    }
}
